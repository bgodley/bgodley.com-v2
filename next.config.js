const { withAxiom } = require("next-axiom")

/** @type {import('next').NextConfig} */
const nextConfig = {
  httpAgentOptions: {
    keepAlive: true
  }
}

module.exports = withAxiom(nextConfig)
