import { motion } from "framer-motion"
import { type PropsWithChildren, type FC } from "react"

interface ScrollAnimationProps {
  delay?: number
}

const ScrollAnimation: FC<PropsWithChildren<ScrollAnimationProps>> = ({ children, delay = 0.1 }) => {
  return <motion.div
    style={{ width: "100%", height: "100%" }}
    viewport={{ once: true }}
    initial={{ opacity: 0, y: 50 }}
    whileInView={{ opacity: 1, y: 0 }}
    transition={{ type: "spring", stiffness: 30, damping: 10, delay }}
  >
    {children}
  </motion.div>
}

export default ScrollAnimation
