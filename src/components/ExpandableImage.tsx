import { ZoomIn } from "@mui/icons-material"
import { Backdrop, Box, Paper } from "@mui/material"
import Image, { type ImageProps } from "next/image"
import { useState, type FC } from "react"

const ExpandableImage: FC<ImageProps> = ({ alt, src, style, ...props }) => {
  const [hover, setHover] = useState<boolean>(false)
  const [expanded, setExpanded] = useState<boolean>(false)

  return (<>
    <Paper
      sx={{
        position: "relative",
        width: "100%",
        aspectRatio: "4 / 3",
        cursor: "pointer",
        backgroundColor: "rgba(0,0,0,.5)"
      }}
      elevation={8}
      onMouseEnter={() => { setHover(true) }}
      onMouseLeave={() => { setHover(false) }}
      onClick={() => { setExpanded(true); setHover(false) }}
    >
      <Box
        sx={{
          position: "absolute",
          width: "100%",
          height: "100%",
          top: 0,
          left: 0,
          backgroundColor: "rgba(0, 0, 0, 0.5)",
          opacity: hover ? 1 : 0,
          zIndex: 1,
          transition: "opacity 0.5s"
        }}
      />
      {hover && (
        <Box
          component={ZoomIn}
          color="white"
          sx={{
            position: "absolute",
            width: "100px",
            height: "100px",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            zIndex: 2
          }}
        />
      )}
      <Image
        src={src}
        alt={alt}
        style={{
          zIndex: 0,
          ...style
        }}
        {...props}
      />
    </Paper>
    <Backdrop
      open={expanded}
      onClick={() => { setExpanded(false) }}
      sx={{
        zIndex: theme => theme.zIndex.drawer + 1
      }}
    >
      <Box
        sx={{
          position: "absolute",
          width: "calc(90vw)",
          maxWidth: theme => theme.breakpoints.values.lg,
          height: "calc(90vh)"
        }}
      >
        <Image
          src={src}
          alt={alt}
          {...props}
          style={{
            objectFit: "contain"
          }}
        />
      </Box>
    </Backdrop>
  </>)
}

export default ExpandableImage
