import { TextField } from "@mui/material"
import { styled, alpha } from "@mui/material/styles"

// const StyledTextField = styled(TextField)(({ theme }) => ({
//   borderRadius: 100,
//   input: {
//     color: "#fff"
//   }
// }))

const StyledTextField = styled(TextField)(({ theme }) => ({
  "& .MuiFilledInput-root": {
    border: "1px solid #e2e2e1",
    overflow: "hidden",
    borderRadius: 5,
    backgroundColor: theme.palette.mode === "light" ? "#fcfcfb" : "#2b2b2b",
    transition: theme.transitions.create([
      "border-color",
      "box-shadow"
    ]),
    "&:hover": {
      backgroundColor: theme.palette.mode === "light" ? "#fff" : "#1c1c1c"
    },
    "&.Mui-focused": {
      backgroundColor: theme.palette.mode === "light" ? "#fff" : "#1c1c1c",
      boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 2px`,
      borderColor: theme.palette.primary.main,
      color: theme.palette.text.secondary
    },
    "::before": {
      display: "none"
    },
    "& .MuiFilledInput-input": {
      color: theme.palette.text.primary
    }
  },
  "& label": {
    color: `${theme.palette.text.secondary} !important`
  }
}))

export default StyledTextField
