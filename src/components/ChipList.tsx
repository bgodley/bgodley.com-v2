import { Box, Chip, type ChipProps } from "@mui/material"
import { type FC } from "react"

interface ChipListProps extends ChipProps {
  chips: string[]
}

const ChipList: FC<ChipListProps> = ({ chips, ...props }) => {
  return (
    <Box mb={2} {...props}>
      {chips.map((chip) => (
        <Chip
          key={chip}
          label={chip}
          sx={{
            mr: 1,
            mb: 0.5,
            backgroundColor: theme => theme.palette.secondary.dark,
            color: theme => theme.palette.secondary.contrastText
          }}
        />
      ))}
    </Box>
  )
}
export default ChipList
