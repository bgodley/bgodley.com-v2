import { Box, Grid, Typography, type BoxProps } from "@mui/material"
import { type StaticImageData } from "next/image"
import React, { type FC, type PropsWithChildren } from "react"
import ExpandableImage from "@/components/ExpandableImage"
import ScrollAnimation from "@/components/ScrollAnimation"
import Image from "next/image"

interface HeroPageProps extends BoxProps {
  image: string | StaticImageData
  expandable?: boolean
  alt: string
  reverse?: boolean
  imageColumns?: number
  sizes?: string
  priority?: boolean
}

const HeroPage: FC<PropsWithChildren<HeroPageProps>> = ({ children, image, alt, sizes, reverse = false, expandable = false, imageColumns = 6, priority, ...props }) => {
  return (
    <Box>
      <Box
        component={Grid}
        container
        spacing={4}
        {...props}
      >
        <Grid item
          xs={12}
          md={12 - imageColumns}
          sx={{
            order: {
              xs: 1,
              md: reverse ? 0 : 1
            }
          }}
        >
          <ScrollAnimation>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                height: "100%"
              }}
            >
              <Box
                sx={{
                  p: {
                    sm: 0,
                    md: 4
                  },
                  py: 4
                }}
              >
                {children}
              </Box>
            </Box>
          </ScrollAnimation>
        </Grid>
        <Grid item
          xs={12}
          md={imageColumns}
          sx={{
            order: {
              xs: 0,
              md: reverse ? 1 : 0
            }
          }}
        >
          <ScrollAnimation delay={0.4}>
            <Box
              sx={{
                position: "relative",
                width: "100%",
                height: "100%",
                minHeight: "300px"
              }}
            >
              {expandable && <>
                <ExpandableImage
                  src={image}
                  alt={alt}
                  sizes={sizes}
                  priority={priority}
                  style={{
                    objectFit: "cover",
                    objectPosition: "top center"
                  }}
                  fill
                />
                <Typography mt={1}>
                  Click to expand
                </Typography>
              </>}
              {!expandable && <Image src={image} alt={alt} sizes={sizes} style={{
                objectFit: "contain"
              }} fill />}
            </Box>
          </ScrollAnimation>
        </Grid>
      </Box>
    </Box>
  )
}

export default HeroPage
