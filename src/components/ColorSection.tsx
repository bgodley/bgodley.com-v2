import { Box, type BoxProps } from "@mui/material"
import { type PropsWithChildren, type FC } from "react"

interface ColorSectionProps extends BoxProps {
  includeTop?: boolean
  includeBottom?: boolean
}

const ColorSection: FC<PropsWithChildren<ColorSectionProps>> = ({ includeTop = true, includeBottom = true, children, sx, ...props }) => {
  return (
    <Box
      component="section"
      sx={{
        bgcolor: theme => theme.palette.secondary.main,
        color: theme => theme.palette.secondary.contrastText,
        position: "relative",
        mt: includeTop ? 6 : 0,
        mb: includeBottom ? 6 : 0,
        py: 8,
        ...sx
      }}
      {...props}
    >
      {includeTop && <Box
        sx={{
          position: "absolute",
          top: theme => theme.spacing(-6),
          width: "100%",
          height: theme => theme.spacing(6),
          backgroundImage: "url(\"/svgs/WavyEdge.svg\")",
          backgroundPosition: "top",
          backgroundRepeat: "repeat-x",
          backgroundSize: "max(100%, 1000px) 100%",
          zIndex: -1
        }}
      />}
      {includeBottom && <Box
        sx={{
          position: "absolute",
          bottom: theme => theme.spacing(-6),
          width: "100%",
          height: theme => theme.spacing(6),
          transform: "scale(-1, -1)",
          backgroundImage: "url(\"/svgs/WavyEdge.svg\")",
          backgroundRepeat: "repeat-x",
          backgroundSize: "max(100%, 1000px) 100%",
          zIndex: -1
        }}
      />}
      {children}
    </Box>
  )
}
export default ColorSection
