import { Button, type ButtonProps } from "@mui/material"
import { styled } from "@mui/material/styles"

const StyledButton = styled((props: ButtonProps) => {
  return (
    <Button
      {...props}
    />
  )
})(({ theme }) => ({
  borderRadius: 100,
  paddingLeft: theme.spacing(4),
  paddingRight: theme.spacing(4)
}))

export default StyledButton
