import React, { useState } from "react"
import { type FC } from "react"
import { Box, Container, Drawer, Hidden, IconButton, Stack, Typography } from "@mui/material"
import MenuIcon from "@mui/icons-material/Menu"
import Link from "next/link"
import StyledButton from "./StyledButton"

const appName = "Braden Godley"
const navLinks = {
  Home: "/",
  Portfolio: "/portfolio",
  "Contact me": "/contact"
}

const NavigationBarMobile: FC = () => {
  const [open, setOpen] = useState<boolean>(false)

  return (<>
    <Box
      sx={{
        backgroundColor: theme => theme.palette.background.default,
        position: "sticky",
        top: theme => theme.spacing(-3),
        zIndex: 100
      }}
    >
      <Container
        maxWidth="lg"
        sx={{
          py: 2,
          minHeight: "100px",
          display: "flex",
          alignItems: "flex-end"
        }}
      >
        <Stack
          width="100%"
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Link href="/">
            <Stack
              direction="row"
              alignItems="center"
              spacing={3}
            >
              <Typography
                color="text.primary"
                variant="h1"
              >
                {appName}
              </Typography>
            </Stack>
          </Link>
          <IconButton
            color="inherit"
            aria-label="open navigation menu"
            onClick={() => { setOpen(true) }}
          >
            <MenuIcon />
          </IconButton>
        </Stack>
      </Container>
    </Box>

    <Drawer
      onClick={() => { setOpen(false) }}
      onClose={() => { setOpen(false) }}
      anchor="top"
      open={open}
    >
      <Stack
        spacing={3}
        p={2}
        sx={{
          backgroundColor: theme => theme.palette.background.default
        }}
      >
        {
          Object.entries(navLinks).map(([text, link], i) => (
            <StyledButton
              fullWidth
              key={text}
              href={link}
              color="secondary"
              variant={i === Object.entries(navLinks).length - 1 ? "contained" : "text"}
            >
              {text}
            </StyledButton>
          ))
        }
      </Stack>
    </Drawer>
  </>)
}

const NavigationBarDesktop: FC = () => {
  return (
    <Box
      sx={{
        backgroundColor: theme => theme.palette.background.default,
        position: "sticky",
        top: theme => theme.spacing(-3),
        zIndex: 100
      }}
    >
      <Container
        maxWidth="lg"
        sx={{
          py: 2,
          minHeight: "100px",
          display: "flex",
          alignItems: "flex-end"
        }}
      >
        <Stack
          width="100%"
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Link href="/#">
            <Stack
              direction="row"
              alignItems="center"
              spacing={3}
            >
              <Typography
                variant="h1"
                color="text.primary"
              >
                {appName}
              </Typography>
            </Stack>
          </Link>
          <Stack
            direction="row"
            spacing={3}
            alignItems="baseline"
          >
            {
              Object.entries(navLinks).map(([text, link], i) => (
                <StyledButton
                  key={text}
                  href={link}
                  color="secondary"
                  variant={i === Object.entries(navLinks).length - 1 ? "outlined" : "text"}
                  sx={{
                    px: 2
                  }}
                >
                  {text}
                </StyledButton>
              ))
            }
          </Stack>
        </Stack>
      </Container>
    </Box>
  )
}

const NavigationBar: FC = () => {
  return <>
    <Hidden mdDown>
      <NavigationBarDesktop />
    </Hidden>
    <Hidden mdUp>
      <NavigationBarMobile />
    </Hidden>
  </>
}

export default NavigationBar
