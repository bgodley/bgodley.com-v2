import { Container, Typography, Paper, Button, Grid, Stack, Box, Hidden } from "@mui/material"
import { type FC } from "react"
import { Email, LinkedIn } from "@mui/icons-material"
import StyledButton from "@/components/StyledButton"
import Link from "next/link"

const ContactFooter: FC = () => {
  return (
    <Box
      sx={{
        position: "relative",
        mt: 8,
        pb: 4
      }}
    >
      <Box
        sx={{
          position: "absolute",
          bottom: "50%",
          width: "100%",
          height: "25%",
          backgroundImage: "url(\"/svgs/ContactFooterWavyEdge.svg\")",
          backgroundPosition: "top",
          backgroundRepeat: "repeat-x",
          backgroundSize: "100% 100%",
          zIndex: -1
        }}
      />
      <Box
        sx={{
          position: "absolute",
          bottom: "0%",
          width: "100%",
          height: "50%",
          backgroundColor: theme => theme.palette.text.secondary,
          zIndex: -1
        }}
      />
      <Container
        maxWidth="md"
      >
        <Paper
          elevation={4}
          sx={{
            bgcolor: theme => theme.palette.secondary.main,
            color: theme => theme.palette.secondary.contrastText,
            borderRadius: "10px",
            p: 4
          }}
        >
          <Grid container>
            <Grid item xs={12} sm={6}>
              <Stack spacing={1}>
                <Typography variant="h3">
                  Contact me
                </Typography>
                <Box>
                  <Button
                    startIcon={
                      <Email color="inherit" />
                    }
                    href="mailto:braden@bgodley.com"
                  >
                    braden@bgodley.com
                  </Button>
                </Box>
                {/* <Box>
                  <Button
                    startIcon={
                      <Phone color="inherit" />
                    }
                    href="tel:541-313-6848"
                  >
                    541-313-6848
                  </Button>
                </Box> */}
                <Box>
                    <Link href="https://www.linkedin.com/in/bgodley/">
                      <Button
                        startIcon={
                          <LinkedIn color="inherit" />
                        }
                      >
                        LinkedIn
                      </Button>
                    </Link>
                  </Box>
                  <Box>
                  <Link href="/contact">
                    <StyledButton
                      aria-label="Get in touch"
                      variant="contained"
                      color="primary"
                    >
                      Get in touch
                    </StyledButton>
                  </Link>
                </Box>
              </Stack>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Hidden smDown>
                <Box
                  sx={{
                    position: "relative",
                    width: "100%",
                    height: "100%"
                  }}
                >
                  <Box
                    component="img"
                    alt="mountain"
                    sx={{
                      position: "absolute",
                      width: "100%",
                      bottom: theme => theme.spacing(-6)
                    }}
                    src={"/svgs/Mountain.svg"}
                  />
                </Box>
              </Hidden>
            </Grid>
          </Grid>
        </Paper>
      </Container>
    </Box>
  )
}
export default ContactFooter
