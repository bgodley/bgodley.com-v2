import { Box, type BoxProps } from "@mui/material"
import Image, { type StaticImageData } from "next/image"
import React from "react"
import { type FC } from "react"

interface StyledImageProps extends BoxProps {
  src: string | StaticImageData
  alt: string
  objectFit?: string
}

const StyledImage: FC<StyledImageProps> = ({ src, alt, sx, objectFit = "cover", ...props }) => {
  return (
    <Box
      sx={{
        height: "100%",
        position: "relative",
        ...sx
      }}
      {...props}
    >
      <Box
        component={Image}
        fill
        alt={alt}
        src={src}
        sx={{
          objectFit
        }}
      />
    </Box>
  )
}
export default StyledImage
