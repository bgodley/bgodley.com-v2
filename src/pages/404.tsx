import ContactFooter from "@/components/ContactFooter"
import HeroPage from "@/components/HeroPage"
import StyledButton from "@/components/StyledButton"
import { Container, Typography } from "@mui/material"
import Head from "next/head"
import Link from "next/link"
import React, { type FC } from "react"
import NavigationBar from "../components/NavigationBar"

const Home: FC = () => {
  return (
    <>
      <Head>
        <title>Page not found | Braden Godley, Eugene, OR</title>
        <meta name="description" content="Page not found" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <NavigationBar />
        <Container maxWidth="lg">
          <HeroPage image={"/svgs/DesignIllustration.svg"} alt={"404 not found"} py={8}>
            <Typography variant="h2" pb={2}>Page not found</Typography>
            <Typography variant="body1" pb={3}>
              {"Sorry about that! Here's a link back to the home page."}
            </Typography>
            <Link href="/">
              <StyledButton
                variant="contained"
                color="secondary"
                aria-label="Go home"
              >
                Go home
              </StyledButton>
            </Link>
          </HeroPage>
        </Container>
      </main>
      <footer>
        <ContactFooter />
      </footer>
    </>
  )
}

export default Home
