import Head from "next/head"
import React, { type FC } from "react"

import NavigationBar from "@/components/NavigationBar"
import LandingSection from "@/sections/LandingSection"
import ContactFooter from "@/components/ContactFooter"

const Home: FC = () => {
  return (
    <>
      <Head>
        <title>Braden Godley | Professional Web Developer - Eugene, Oregon</title>
        <meta name="description" content="Braden Godley is a freelance professional web developer located in Eugene, Oregon." />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="canonical" href="https://bgodley.com/" />

        <meta property="og:title" content="Braden Godley, Professional Web Developer in Eugene Oregon" />
        <meta property="og:description" content={`Hi! I'm Braden Godley. I design, create, and host custom websites with the goal of making the entire process from start to finish as easy as possible. 
          Let me handle the technical details so you can focus on what's important: running your business.`} />
        <meta property="og:url" content="https://bgodley.com/" />
        <meta property="og:image" content="https://bgodley.com/opengraph-image.png" />
      </Head>
      <main>
        <NavigationBar />
        <LandingSection />
        {/* <ServicesSection /> */}
        {/* <AboutSection /> */}
        <ContactFooter />
      </main>
      <footer>
      </footer>
    </>
  )
}

export default Home
