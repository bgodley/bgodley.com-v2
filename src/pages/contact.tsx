import Head from "next/head"
import React, { type FormEvent, useState, type FC } from "react"
import { useRouter } from "next/router"
// import Image from "next/image"
import { Container, Typography, Box, Stack, MenuItem, CircularProgress, Paper, Grid, Button } from "@mui/material"
import { Email, LinkedIn } from "@mui/icons-material"

import NavigationBar from "@/components/NavigationBar"
import StyledButton from "@/components/StyledButton"
import ColorSection from "@/components/ColorSection"
import TextField from "@/components/StyledTextField"

import Link from "next/link"

const Home: FC = () => {
  const router = useRouter()
  const [submissionState, setSubmissionState] = useState<undefined | "pending" | "error">()

  const onSubmit = async (e: FormEvent): Promise<void> => {
    e.preventDefault()

    setSubmissionState("pending")

    const form = e.target as HTMLFormElement
    const formData = new FormData(form)

    const data = Object.fromEntries(formData.entries())
    const json = JSON.stringify(data)

    const response = await fetch("/api/submitContactForm", {
      method: "POST",
      body: json,
      headers: {
        "Content-Type": "application/json"
      }
    })

    if (response.ok) {
      void router.push("/success")
    } else {
      setSubmissionState("error")
    }
  }

  return (
    <>
      <Head>
        <title>Contact me | Braden Godley - Professional Web Developer</title>
        <meta name="description" content="Let's get in touch! I'm always looking for opportunites to work with great people." />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="canonical" href="https://bgodley.com/contact" />

        <meta property="og:title" content="Contact me" />
        <meta property="og:description" content="Let's get in touch! I'm always looking for opportunites to work with great people." />
        <meta property="og:url" content="https://bgodley.com/contact" />
        <meta property="og:image" content="https://bgodley.com/opengraph-image.png" />
      </Head>
      <main>
        <NavigationBar />
        <Container maxWidth="lg" sx={{ py: 8 }}>
          <Typography variant="h2" py={4} textAlign="center">
            {"Let's get in touch!"}
          </Typography>
          <Typography variant="body1" pb={3} textAlign="center">
            {"I'm always looking for new opportunities to work with great people."}
          </Typography>
          <Typography variant="body1" pb={3} textAlign="center">
            {"Fill out the form below and I'll respond to you within the next 24 hours."}
          </Typography>
        </Container>
        <ColorSection
          includeBottom={false}
        >
          <Container maxWidth="lg">
            <Typography variant="h2" gutterBottom>
              Send me a message
            </Typography>
            <Grid container spacing={4}>
              <Grid item xs={12} md={6}>
                <Stack
                  component="form"
                  onSubmit={(e) => { void onSubmit(e) }}
                  spacing={2}

                  sx={{
                    color: "#fff"
                  }}
                >

                  <TextField
                    name="name"
                    type="text"
                    label="Name"
                    placeholder="John Doe"
                    variant="filled"
                    required
                  />

                  <TextField
                    name="email"
                    type="email"
                    label="Email"
                    placeholder="john@doe.com"
                    variant="filled"
                    required
                  />

                  <TextField
                    name="company"
                    type="text"
                    label="Company"
                    placeholder="Your Company Inc."
                    variant="filled"
                  />

                  <TextField
                    name="inquiry"
                    type="text"
                    label="Inquiry"
                    placeholder="What can I do for you?"
                    variant="filled"
                    select
                    required
                  >
                    <MenuItem value="Full Stack Development">Full Stack Development</MenuItem>
                    <MenuItem value="DevOps">DevOps</MenuItem>
                    <MenuItem value="Other">Other</MenuItem>
                  </TextField>

                  <TextField
                    name="message"
                    type="text"
                    label="Message"
                    placeholder="Provide details about your project."
                    variant="filled"
                    multiline
                    rows={5}
                    required
                  />
                  <Box>
                    <StyledButton
                      aria-label="Send message"
                      variant="contained"
                      color="primary"
                      type="submit"
                      disabled={submissionState === "pending"}
                      startIcon={submissionState === "pending" ? <CircularProgress size={20} /> : undefined}
                    >
                      Send message
                    </StyledButton>
                  </Box>
                  {submissionState === "error" && <Paper>
                    <Typography variant="body1" p={2}>
                      Form submission failed. Please check your inputs for errors and try again.
                    </Typography>
                  </Paper>}
                </Stack>
              </Grid>
              <Grid item xs={12} md={6}>
                <Stack spacing={1}>
                  <Typography variant="body1">
                    If you prefer, I am also reachable via email or LinkedIn.
                    Please feel free to reach out to me with any questions or inquiries.
                  </Typography>
                  <Box>
                    <Button
                      startIcon={
                        <Email color="inherit" />
                      }
                      href="mailto:braden@bgodley.com"
                    >
                      braden@bgodley.com
                    </Button>
                  </Box>
                  {/* <Box>
                    <Button
                      startIcon={
                        <Phone color="inherit" />
                      }
                      href="tel:541-313-6848"
                    >
                      541-313-6848
                    </Button>
                  </Box> */}
                  <Box>
                    <Link href="https://www.linkedin.com/in/bgodley/">
                      <Button
                        startIcon={
                          <LinkedIn color="inherit" />
                        }
                      >
                        LinkedIn
                      </Button>
                    </Link>
                  </Box>
                </Stack>
              </Grid>
            </Grid>
          </Container>
        </ColorSection>
      </main>
      <footer>
      </footer>
    </>
  )
}

export default Home
