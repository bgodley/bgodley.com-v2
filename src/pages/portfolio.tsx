import Head from "next/head";
import { Container, IconButton, Typography, Box } from "@mui/material";
import React, { type FC } from "react";
import Link from "next/link";
import { FormatQuote, KeyboardArrowDown } from "@mui/icons-material";
import Image from "next/image";

import NavigationBar from "@/components/NavigationBar";
import ContactFooter from "@/components/ContactFooter";
import HeroPage from "@/components/HeroPage";
import StyledButton from "@/components/StyledButton";
import ColorSection from "@/components/ColorSection";
import ChipList from "@/components/ChipList";

const Portfolio: FC = () => {
  return (
    <>
      <Head>
        <title>Portfolio | Braden Godley - Professional Web Developer</title>
        <meta
          name="description"
          content="Braden Godley is a freelance professional web developer located in Eugene, Oregon."
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="canonical" href="https://bgodley.com/portfolio" />

        <meta property="og:title" content="View my portfolio" />
        <meta
          property="og:description"
          content={`Take a look at some of my recent projects, from commercial web development to AI chatbot applications.
          I have extensive experience coding in Next.js, React, and Node.js and I'm always looking for new challenges.`}
        />
        <meta property="og:url" content="https://bgodley.com/portfolio" />
        <meta
          property="og:image"
          content="https://bgodley.com/opengraph-image.png"
        />
      </Head>
      <main>
        <NavigationBar />
        <Container
          maxWidth="lg"
          sx={{
            minHeight: "500px",
            maxHeight: "100vh",
            aspectRatio: "16/9",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Box>
            <Box
              display="flex"
              justifyContent="center"
              alignItems="center"
              mb={4}
            >
              <Box
                sx={{
                  position: "relative",
                  width: "75%",
                  aspectRatio: "1/1",
                }}
              >
                <Image
                  src={"/images/Headshot.webp"}
                  fill
                  priority
                  style={{
                    objectPosition: "center",
                  }}
                  alt="Braden Godley"
                />
              </Box>
            </Box>
            <Typography variant="h2" textAlign="center" pb={4}>
              Project Portfolio
            </Typography>
            <Typography variant="body1" textAlign="center">
              Take a glance into my recent work
              <IconButton
                color="secondary"
                sx={{ ml: 2 }}
                onClick={() => {
                  const portfolioStart =
                    document.getElementById("portfolio-start");
                  portfolioStart?.scrollIntoView({ behavior: "smooth" });
                }}
              >
                <KeyboardArrowDown sx={{ fontSize: "32px" }} />
              </IconButton>
            </Typography>
          </Box>
        </Container>

        <ColorSection>
          <Box
            id="portfolio-start"
            sx={{ position: "absolute", top: (theme) => theme.spacing(-20) }}
          />
          <Container maxWidth="lg">
            <HeroPage
              expandable
              image={"/images/noticeflow.png"}
              alt="ChatJeopardy"
            >
              <Typography variant="h4" color="text.secondary" pb={2}>
                Full Stack Web Development
              </Typography>
              <Typography variant="h2" pb={2}>
                NoticeFlow
              </Typography>
              <ChipList
                chips={[
                  "React",
                  "Material UI",
                  "Next.js",
                  "tRPC",
                  "Prisma",
                  "NextAuth",
                ]}
              />
              <Typography variant="body1" pb={2}>
                NoticeFlow is an open-source and free web application designed
                to help universities and other organizations handle DMCA
                complaints.
              </Typography>
              <Typography variant="body1" pb={2}>
                I designed NoticeFlow because I didn&apos;t want to maintain our
                Perl based in-house solution to handling DMCA notices. I hope that this
                application can provide an alternative to in-house solutions for this problem.
              </Typography>
              <Typography variant="body1">
                NoticeFlow offers the following features and more:
              </Typography>

              <ul>
                <Typography component="li">
                  Pull DMCA complaint emails from an inbox using IMAP,
                  automatically parsing ACNS XML data and ingesting into the
                  database.
                </Typography>
                <Typography component="li">
                  Lightweight ticketing system used specifically to track and
                  manage notices.
                </Typography>
                <Typography component="li">
                  Automatically perform a lookup when a DMCA notice is received
                  to investigate the violation.
                </Typography>
                <Typography component="li">
                  Ability to issue network disables when a user is found to be
                  in violation of DMCA.
                </Typography>
                <Typography component="li">
                  View detailed analytics over varying timespans to increase
                  managerial visibility into operations.
                </Typography>
                <Typography component="li">
                  Send emails to complainants using prebuilt templates that
                  cover most scenarios.
                </Typography>
                <Typography component="li">
                  SAML authentication and role-based access control
                </Typography>
              </ul>

              <Typography variant="body1" mb={2}>
                Check out the website for NoticeFlow to learn more
              </Typography>
              <Link href="https://noticeflow.com">
                <StyledButton
                  variant="contained"
                  color="primary"
                  aria-label="View website"
                >
                  View website
                </StyledButton>
              </Link>
            </HeroPage>
          </Container>
        </ColorSection>

        <Container maxWidth="lg">
          <HeroPage
            py={12}
            expandable
            image={"/images/secret-exchange.png"}
            alt="WebStrata Secret Exchange"
            reverse
          >
            <Typography variant="h4" color="text.secondary" pb={2}>
              Full Stack Web Development
            </Typography>
            <Typography variant="h2" pb={2}>
              WebStrata Secret Exchange
            </Typography>
            <ChipList
              chips={[
                "React",
                "Material UI",
                "Next.js",
                "AWS S3",
                "Cryptography",
              ]}
            />
            <Typography>
              I had the pleasure of designing and coding a web application for
              WebStrata Internet Services that allows them to securely exchange
              secrets and credentials with web hosting clients.
            </Typography>
            <ul>
              <Typography component="li">
                Securely encrypts and decrypts secrets using AES-256 encryption
                and a passphrase.
              </Typography>
              <Typography component="li">
                A link to the secret is generated which can be sent to the
                client.
              </Typography>
              <Typography component="li">
                Once a secret is unlocked, it is deleted and cannot be viewed
                again.
              </Typography>
            </ul>

            <Link href="https://secret.webstrata.com">
              <StyledButton
                variant="contained"
                color="secondary"
                aria-label="View website"
              >
                View website
              </StyledButton>
            </Link>
          </HeroPage>
        </Container>
        <ColorSection>
          <Container maxWidth="lg">
            <HeroPage
              expandable
              image={"/images/adventure-chat.png"}
              alt="Adventure Chat"
            >
              <Typography variant="h4" color="text.secondary" pb={2}>
                AI Chatbot Development
              </Typography>
              <Typography variant="h2" pb={2}>
                Adventure Chat
              </Typography>
              <ChipList
                chips={["React", "Material UI", "Next.js", "ChatGPT"]}
              />
              <Typography pb={2}>
                Adventure Chat is a next-generation choose your own adventure
                game that uses ChatGPT to create unique stories.
              </Typography>
              <Typography pb={2}>
                You choose a character to play as and the chatbot will generate
                a scenario for you to take part in. The story evolves as you
                take actions, allowing for a unique experience every time.
              </Typography>
              <Typography pb={2}>
                Behind the scenes, the application uses ChatGPT not only for
                generating the story, but also for validating and categorizing
                user inputs.
              </Typography>

              <Link href="https://adventurechat.app">
                <StyledButton
                  variant="contained"
                  color="primary"
                  aria-label="Try it out"
                >
                  Try it out
                </StyledButton>
              </Link>
            </HeroPage>
          </Container>
        </ColorSection>
        <Container maxWidth="lg">
          <HeroPage
            expandable
            image={"/images/fredericksburg-bbq.png"}
            py={12}
            alt="Fredericksburg BBQ Cleaning"
            reverse
          >
            <Typography variant="h4" color="text.secondary" pb={2}>
              Commercial Web Development
            </Typography>
            <Typography variant="h2" pb={2}>
              Fredericks BBQ Cleaning
            </Typography>
            <ChipList chips={["React", "Material UI", "Next.js"]} />
            <Typography pb={2}>
              Created a professional business website for a fictional BBQ
              cleaning business in Fredericksburg. The website is built using
              Next.js and follows responsive web design principles.
              <br />
              <br />
              Created design in Figma first, and then implemented the website in
              code.
            </Typography>

            <Link href="https://fredericksburg-bbq.vercel.app/">
              <StyledButton
                variant="contained"
                color="secondary"
                aria-label="View website"
              >
                View website
              </StyledButton>
            </Link>
          </HeroPage>
        </Container>

        <ColorSection>
          <Box
            sx={{ position: "absolute", top: (theme) => theme.spacing(-20) }}
          />
          <Container maxWidth="lg">
            <HeroPage
              expandable
              image={"/images/chat-jeopardy.png"}
              alt="ChatJeopardy"
            >
              <Typography variant="h4" color="text.secondary" pb={2}>
                Full Stack Web Development
              </Typography>
              <Typography variant="h2" pb={2}>
                ChatJeopardy
              </Typography>
              <ChipList
                chips={[
                  "React",
                  "TailwindCSS",
                  "Next.js",
                  "tRPC",
                  "Prisma",
                  "NextAuth",
                ]}
              />
              <Typography variant="body1" pb={2}>
                ChatJeopardy was inspired by my interest in the gameshow
                Jeopardy, although it is currently not very much like actual
                Jeopardy. Most trivia video games require contestants to answer
                multiple choice questions, but ChatJeopardy allows you to type
                out your answer without restricting you to a certain format.
              </Typography>
              <Typography variant="body1" pb={2}>
                This project was my first full-stack application built with the
                T3 stack. Since then I have worked on a couple of full-stack
                production applications using this tech stack.
              </Typography>
              <Link href="https://chat-jeopardy.vercel.app">
                <StyledButton
                  variant="contained"
                  color="primary"
                  aria-label="View website"
                >
                  Play now
                </StyledButton>
              </Link>
            </HeroPage>
          </Container>
        </ColorSection>

        <Container maxWidth="lg">
          <HeroPage
            expandable
            py={12}
            image={"/images/taskpilot.png"}
            alt="TaskPilot"
            reverse
          >
            <Typography variant="h4" color="text.secondary" pb={2}>
              Full Stack Web Development
            </Typography>
            <Typography variant="h2" pb={2}>
              {"TaskPilot"}
            </Typography>
            <ChipList
              chips={[
                "React",
                "Material UI",
                "MySQL",
                "Express.js",
                "Kubernetes",
              ]}
            />
            <Typography pb={2}>
              I created TaskPilot originally for myself to keep track of my
              daily life. However, now the site is open to the public and anyone
              can create an account and start using it.
            </Typography>
            <Typography pb={2}>
              TaskPilot is a task management application that allows you to
              create tasks, assign them to different categories, and track your
              progress.
            </Typography>
            <Typography pb={2}>
              TaskPilot was built using React and Material UI for the frontend
              and Express and MySQL for the backend. The authentication system
              was custom coded by me using what I&apos;ve learned about JSON web
              tokens, cookies, and secure salting and hashing of passwords.
            </Typography>

            <Link href="https://task-bg.com/">
              <StyledButton
                variant="contained"
                color="secondary"
                aria-label="Try it out"
              >
                Try it out
              </StyledButton>
            </Link>
          </HeroPage>
        </Container>
        <ColorSection>
          <Container maxWidth="lg">
            <HeroPage
              expandable
              image={"/images/coach-coutinho.png"}
              alt="Coach Coutinho"
            >
              <Typography variant="h4" color="text.secondary" pb={2}>
                Commercial Web Development
              </Typography>
              <Typography variant="h2" pb={2}>
                League of Legends Coaching Site
              </Typography>
              <ChipList
                chips={["React", "Material UI", "Express", "Nodemailer"]}
              />
              <Typography variant="body1" pb={2}>
                Built a website for a League of Legends coach to help him grow
                his client base. Generated a significant increase in leads by
                funneling customers to the coach&apos;s Discord server.
              </Typography>
              <Typography variant="body2" color="text.secondary">
                <FormatQuote
                  fontSize="small"
                  sx={{ transform: "rotate(180deg)" }}
                />
                <i>{`Braden G. made a project for me, a coaching site for a game called "League of Legends".
                I really liked the work, it met all my expectations and more. I would hire him for any other type of work,
                very attentive, meets all our demands and always willing to change anything that is in our interest. 
                I highly recommend!`}</i>
                <FormatQuote fontSize="small" />
              </Typography>
              <Typography variant="body2" color="inherit">
                -Lucas Coutinho, Business Owner
              </Typography>
              <Typography variant="body2" color="inherit" pb={2}>
                <i>via Upwork</i>
              </Typography>
              <Link href="https://coachcoutinho.com">
                <StyledButton
                  variant="contained"
                  color="primary"
                  aria-label="View website"
                >
                  View website
                </StyledButton>
              </Link>
            </HeroPage>
          </Container>
        </ColorSection>

        <Container maxWidth="lg">
          <HeroPage
            expandable
            image={"/images/titos-burritos.png"}
            py={12}
            alt="Tito's Burritos & Bites"
          >
            <Typography variant="h4" color="text.secondary" pb={2}>
              Commercial Web Development
            </Typography>
            <Typography variant="h2" pb={2}>
              {"Tito's Burritos & Bites"}
            </Typography>
            <ChipList
              chips={["Strapi CMS", "React", "Material UI", "Next.js"]}
            />
            <Typography pb={2}>
              I created this website for a fictional food truck using Next.js
              and Strapi.
              <br />
              <br />
              {`Combining these two technologies allows you to make quick edits to content through
              Strapi's admin dashboard while still being able to leverage the incredible speed
              and search engine optimization that Next.js offers.`}
            </Typography>

            <Link href="https://titos-burritos-and-bites.vercel.app/">
              <StyledButton
                variant="contained"
                color="secondary"
                aria-label="View website"
              >
                View website
              </StyledButton>
            </Link>
          </HeroPage>
        </Container>

        <ColorSection>
          <Container maxWidth="lg">
            <HeroPage
              reverse
              expandable
              image={"/images/chill-screen.png"}
              alt="Chill Screen"
              pt={12}
            >
              <Typography variant="h4" color="text.secondary" pb={2}>
                Web 3D Graphics
              </Typography>
              <Typography variant="h2" pb={2}>
                Chill Screensaver
              </Typography>
              <ChipList chips={["React", "WebGL"]} />
              <Typography variant="body1" pb={2}>
                After learning how to use OpenGL (and consequently WebGL as
                well) I decided to create an aesthetically pleasing screensaver
                to test my skills.
              </Typography>
              <Link href="https://chill-screen.vercel.app">
                <StyledButton
                  variant="contained"
                  color="primary"
                  aria-label="View website"
                >
                  View website
                </StyledButton>
              </Link>
            </HeroPage>
          </Container>
        </ColorSection>

        <Box sx={{ py: 6 }} />
      </main>
      <footer>
        <ContactFooter />
      </footer>
    </>
  );
};

export default Portfolio;
