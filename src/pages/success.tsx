import HeroPage from "@/components/HeroPage"
import StyledButton from "@/components/StyledButton"
import { Container, Typography } from "@mui/material"
import Head from "next/head"
import Link from "next/link"
import React, { type FC } from "react"
import NavigationBar from "../components/NavigationBar"

const Success: FC = () => {
  return (
    <>
      <Head>
        <title>Form submitted! | Braden Godley - Professional Web Developer</title>
        <meta name="description" content="Successfully submitted contact form. I'll respond to you shortly." />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="canonical" href="https://bgodley.com/success" />
      </Head>
      <main>
        <NavigationBar />
        <Container maxWidth="lg">
          <HeroPage image={"/svgs/MailIllustration.svg"} alt={"Successfully sent message"} py={8}>
            <Typography variant="h2" pb={2}>Success!</Typography>
            <Typography variant="body1" pb={3}>
              {`Thank you for taking the time to reach out to me.
              I'll respond to you within the next 24 hrs about your inquiry.`}
            </Typography>
            <Link href="/">
              <StyledButton variant="contained" aria-label="Go home" color="secondary">
                Go home
              </StyledButton>
            </Link>
          </HeroPage>
        </Container>
      </main>
    </>
  )
}

export default Success
