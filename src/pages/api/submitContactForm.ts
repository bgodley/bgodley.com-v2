// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next"
import nodemailer from "nodemailer"
import { z } from "zod"

const EMAIL_HOST = process.env.EMAIL_HOST
const EMAIL_USER = process.env.EMAIL_USER
const EMAIL_TO = process.env.EMAIL_TO
const EMAIL_PASSWORD = process.env.EMAIL_PASSWORD

if (EMAIL_HOST !== undefined) throw new Error("EMAIL_HOST is undefined")
if (EMAIL_USER !== undefined) throw new Error("EMAIL_USER is undefined")
if (EMAIL_TO !== undefined) throw new Error("EMAIL_TO is undefined")
if (EMAIL_PASSWORD !== undefined) throw new Error("EMAIL_PASSWORD is undefined")

interface FormSubmissionRequest {
  name: string
  email: string
  company?: string
  inquiry: string
  message: string
}

const formSubmissionRequestSchema = z.object({
  name: z.string().min(1).max(100).trim(),
  email: z.string().email().max(100).trim(),
  company: z.string().min(1).max(100).optional(),
  inquiry: z.enum(["Full Stack Development", "DevOps", "Other"]),
  message: z.string().min(1).max(4000).trim()
})

export default async function handler (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> {
  if (req.method !== "POST") {
    res.send(405)
    return
  }

  const request = req.body as FormSubmissionRequest
  try {
    formSubmissionRequestSchema.parse(request)
  } catch (err) {
    console.error(err)
    res.send(400)
    return
  }

  const transporter = nodemailer.createTransport({
    host: EMAIL_HOST,
    port: 465,
    secure: true,
    requireTLS: true,
    auth: {
      user: EMAIL_USER,
      pass: EMAIL_PASSWORD
    },
    tls: {
      rejectUnauthorized: true,
      requestCert: true
    }
  })

  const sendToMe = new Promise<void>((resolve, reject) => {
    const subject = "bgodley.com: Form Submission"
    const body = `
      <h1>Form Submission</h1>
      <p>Name: ${request.name}</p>
      <p>Email: ${request.email}</p>
      <p>Company: ${request.company ?? "N/A"}</p>
      <p>Inquiry: ${request.inquiry}</p>
      <p>Message: ${request.message}</p>
    `
    const mailOptions = {
      from: "\"Braden Godley\" <braden@bgodley.com>",
      to: EMAIL_TO,
      subject,
      html: body,
      text: body,
      headers: {
        "Reply-To": request.email
      }
    }

    transporter.sendMail(mailOptions)
      .then(() => {
        resolve()
      })
      .catch((err) => {
        reject(err)
      })
  })

  const sendToClient = new Promise<void>((resolve, reject) => {
    const subject = "Thank you for your message!"
    const body = `
      <h1>Thank you for your message!</h1>
      <p>I appreciate you taking the time out of your day to contact me. I will get back to you within the next 24 hours.</p>
      <p>Talk to you soon!</p>
      <p>Braden Godley</p>
    `
    const mailOptions = {
      from: "\"Braden Godley\" <braden@bgodley.com>",
      to: request.email,
      subject,
      html: body,
      text: body,
      headers: {
        "Reply-To": EMAIL_TO ?? ""
      }
    }

    transporter.sendMail(mailOptions)
      .then(() => {
        resolve()
      })
      .catch((err) => {
        reject(err)
      })
  })

  try {
    await Promise.all([sendToMe, sendToClient])

    console.log("Successfully received form submission")
    res.send(200)

    return
  } catch (err) {
    console.error(err)
    res.send(500)
  }
}
