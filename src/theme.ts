import { createTheme, responsiveFontSizes, type ThemeOptions } from "@mui/material/styles"
import { deepmerge } from "@mui/utils"
import { Poppins, Lora } from "@next/font/google"

const poppins = Poppins({ display: "swap", subsets: ["latin"], weight: ["400", "500", "600", "700"] })
const lora = Lora({ display: "swap", subsets: ["latin"], weight: ["400", "500", "600", "700"] })

const themeBase: ThemeOptions = {
  typography: {
    fontFamily: [
      poppins.style.fontFamily,
      "-apple-system",
      "BlinkMacSystemFont",
      "\"Segoe UI\"",
      "\"Roboto\"",
      "\"Helvetica\"",
      "\"Arial\"",
      "sans-serif",
      "\"Apple Color Emoji\"",
      "\"Segoe UI Emoji\"",
      "\"Segoe UI Symbol\""
    ].join(","),
    h1: {
      fontFamily: lora.style.fontFamily,
      fontSize: 30,
      lineHeight: 1,
      fontWeight: 600
    },
    h2: {
      fontFamily: lora.style.fontFamily,
      fontSize: 40,
      lineHeight: 1,
      fontWeight: 700
    },
    h3: {
      fontFamily: lora.style.fontFamily,
      fontSize: 24,
      lineHeight: 1,
      fontWeight: 600
    },
    h4: {
      fontSize: 20,
      fontWeight: 600,
      textTransform: "uppercase"
    },
    body1: {
      fontSize: 16,
      fontWeight: 400
    },
    body2: {
      fontSize: 14,
      fontWeight: 400
    },
    button: {
      fontSize: 16,
      fontWeight: 500,
      textTransform: "none"
    }
  },
  // shadows: Array(25).fill("none"),
  shape: {
    borderRadius: 0
  },
  components: {
    MuiButton: {
      defaultProps: {
        disableRipple: true
      },
      styleOverrides: {
        root: ({ ownerState }) => {
          if (ownerState.variant === "text") {
            return {
              backgroundColor: "transparent",
              ":hover": {
                backgroundColor: "transparent"
              }
            }
          } else {
            return {}
          }
        }
      }
    },
    MuiLink: {
      styleOverrides: {
        root: {
          color: "inherit",
          textDecoration: "none"
        }
      }
    }
  }
}

const palette: ThemeOptions = {
  palette: {
    mode: "light",
    primary: {
      main: "#FFF",
      contrastText: "#333"
    },
    secondary: {
      main: "#5628D2",
      contrastText: "#FFF"
    },
    text: {
      primary: "#333",
      secondary: "#A39FFF"
    },
    common: {
      white: "#FFF",
      black: "#020202"
    },
    background: {
      paper: "#fafafa",
      default: "#fafafa"
    }
  }
}

const theme = responsiveFontSizes(createTheme(deepmerge(themeBase, palette)))

export { theme }
