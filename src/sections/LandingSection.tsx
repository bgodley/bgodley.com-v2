import HeroPage from "@/components/HeroPage"
import StyledButton from "@/components/StyledButton"
import { Container, Typography, Box } from "@mui/material"
import Link from "next/link"
import { type FC } from "react"

const LandingSection: FC = () => {
  return (
    <Container
      maxWidth="lg"
    >
      <HeroPage
        image={"/images/Headshot.png"}
        sizes="(max-width: 900px) 560px, 320px"
        priority
        alt={"Braden Godley, Freelance Web Developer in Eugene, Oregon"}
        minHeight="80lvh"
        py={4}
        imageColumns={5}
      >
        <Typography variant="h2" pb={2}>
          Full Stack Developer in Eugene, OR
        </Typography>
        <Typography variant="body1" pb={3}>
          Hi, I&apos;m Braden Godley! I&apos;m a software engineer in Eugene whose main interests are in full stack development and DevOps.
        </Typography>
        <Typography variant="body1" pb={3}>
          Currently, I&apos;m working as a student DevOps engineer for the University of Oregon&apos;s Information Security Office.
        </Typography>
        <Typography variant="body1" pb={3}>
          If you&apos;re interested in my recent projects, please take a look at my portfolio!
        </Typography>
        <Box>
          <Link href="/portfolio">
            <StyledButton
              variant="contained"
              color="secondary"
              aria-label="Portfolio"
            >
              {"View my portfolio"}
            </StyledButton>
          </Link>
        </Box>
      </HeroPage>
    </Container>
  )
}
export default LandingSection
