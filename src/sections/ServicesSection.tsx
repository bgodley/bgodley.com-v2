import HeroPage from "@/components/HeroPage"
import { Container, Stack, Typography } from "@mui/material"
import { type FC } from "react"

import ColorSection from "@/components/ColorSection"

const ServicesSection: FC = () => {
  return (
    <ColorSection>
      <Container>
        <Typography
          variant="h2"
          textAlign="center"
          sx={{
            py: 4
          }}
        >
          Everything your business needs<br />
          to succeed online
        </Typography>
        <Stack
          spacing={16}
        >
          <HeroPage
            image={"/svgs/DesignIllustration.svg"}
            alt={"Web Design"}
            my={4}
            reverse
          >
            <Typography variant="h4" color="text.secondary" pb={1}>
              Web Design
            </Typography>
            <Typography variant="h2" pb={2}>
              Excellent design wins clients
            </Typography>
            <Typography pb={2}>
              I create high-quality products that follow tested design principles to drive user engagement.
            </Typography>
            <Typography pb={2}>
              Every project I work on begins with a professional mockup in Figma that covers all the most.
              important pages in your website.
            </Typography>
            <Typography>
              Starting with a high-fidelity mockup ensures that <b>what I deliver is what you expect.</b>
            </Typography>
          </HeroPage>

          <HeroPage
            image={"/svgs/CodingIllustration.svg"}
            alt={"Custom Development"}
          >
            <Typography variant="h4" color="text.secondary" pb={1}>
              Custom Development
            </Typography>
            <Typography variant="h2" pb={2}>
              Sites that fit your needs
            </Typography>
            <Typography pb={2}>
              I focus on custom-developed websites and applications, using the technology that best
              suits your needs.
            </Typography>
            <Typography pb={2}>
              The sites that I create are highly customizable, lightning-fast, and secure.
            </Typography>
            <Typography>
              Your website will benefit from focused development that fits its exact requirements.{" "}
              <b>No more headaches and half-baked solutions.</b>
            </Typography>
          </HeroPage>

          <HeroPage
            image={"/svgs/ServerIllustration.svg"}
            alt={"Setup & Hosting"}
            reverse
          >
            <Typography variant="h4" color="text.secondary" pb={1}>
              Setup & Hosting
            </Typography>
            <Typography variant="h2" pb={2}>
              Easy hosting saves you time
            </Typography>
            <Typography pb={2}>
              I can provide setup & long-term web hosting for all of the websites I create.
            </Typography>
            <Typography>
              With hands-on experience hosting dozens of websites, databases, APIs, and full-stack web applications,
              I can make sure your website is put online in the <b>most cost-effective and reliable way possible.</b>
            </Typography>
          </HeroPage>
        </Stack>
      </Container>
    </ColorSection>
  )
}

export default ServicesSection
