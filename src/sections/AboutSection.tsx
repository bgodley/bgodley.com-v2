import HeroPage from "@/components/HeroPage"
import StyledButton from "@/components/StyledButton"
import { Container, Typography } from "@mui/material"
import Link from "next/link"
import { type FC } from "react"

const AboutSection: FC = () => {
  return (
    <Container maxWidth="lg">
      <HeroPage
        py={4}
        image={"/images/Headshot2.png"}
        alt="Braden Godley"
        sizes="(max-width: 1200px) 373px, (max-width: 900px) 461px, (max-width: 400px) 200px, 200px"
        imageColumns={4}
      >
        <Typography variant="h4" color="text.secondary" pb={1}>
          About me
        </Typography>
        <Typography variant="h2" pb={2}>
          {"Hi, I'm Braden Godley"}
        </Typography>
        <Typography pb={4}>
          {"I'm a professional web developer located in Eugene, Oregon."}
          <br /><br />
          {`I consider myself a highly motivated learner.
          You can almost always find me at work learning a new skill that I can use to make results.
          I've spent countless hours honing my skills in coding, design, and server administration.`}
          <br /><br />
          I decided to become a freelance web developer so that I could follow my passion of making websites that are both beautiful and useful.
        </Typography>
        <Link href="/portfolio">
          <StyledButton variant="contained" color="secondary" aria-label="View my portfolio">
            View my portfolio
          </StyledButton>
        </Link>
      </HeroPage>
    </Container>
  )
}
export default AboutSection
